      SUBROUTINE rk4(y,dydx,n,x,h,yout,derivs)
      INTEGER n,NMAX
      REAL*8 h,x,dydx(n),y(n),yout(n)
      EXTERNAL derivs
      PARAMETER (NMAX=50)
      INTEGER i
      REAL*8 h6,hh,xh,dym(NMAX),dyt(NMAX),yt(NMAX)
      hh=h*0.5d0
      h6=h/6.d0
      xh=x+hh
      call derivs(x,y,dydx,n)
      do i=1,n
        yt(i)=y(i)+hh*dydx(i)
      end do
      call derivs(xh,yt,dyt,n)
      do i=1,n
        yt(i)=y(i)+hh*dyt(i)
      end do
      call derivs(xh,yt,dym,n)
      do i=1,n
        yt(i)=y(i)+h*dym(i)
        dym(i)=dyt(i)+dym(i)
      end do
      call derivs(x+h,yt,dyt,n)
      do i=1,n
        yout(i)=y(i)+h6*(dydx(i)+dyt(i)+2.d0*dym(i))
      end do
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 0(9p#31&#5(+.
