      subroutine derivsR(x,y,dydx,n)
      use parms, only: g0B,g0S, etaS, etaB,nstep, tau, krate, beta, K
      implicit none
      integer n,i
      double precision x,y(n),dydx(n)
      double precision grow,totalB
      grow=1.d0
      totalB=y(3)
      do i=1,nstep
         totalB=totalB+y(4+i)
      end do
c     c     nutrient, not used
      dydx(1)=-grow*(g0B*y(2)+g0S*y(3))
c     c     resistant (not in totalB for phage sdsorption)
      dydx(2)=g0B*grow*y(2)
c     c     sensitive
      dydx(3)=g0S*grow*y(3)-etaS*y(3)*y(4)
c     c     phage
      dydx(4)=beta*krate*y(5+nstep-1)-etaS*y(4)*totalB      
c     c     infected
      dydx(5)=etaS*y(4)*y(3)-krate*y(5)
      do i=1,nstep-1
         dydx(5+i)=krate*(y(5+i-1)-y(5+i))
      end do
      return 
      end
