      use parms, ONLY: g0B,g0S, etaS, etaB,nstep, tau, krate, beta, K
      implicit none
      integer i,j,ii,jj,n,ifile
      double precision Ninitial, Iinitial,Sinitial,xfrac
      double precision Ifinal, Sfinal
      EXTERNAL derivs,derivsR,rk4
      integer ndata
      double precision totalB,tfinal,dt
      parameter(n=8+4)
      double precision t,dydt(n),y(n),yout(n),trec,tnow
      double precision yrecNP(n),yrecWPI(n),yrecWPR(n)
      integer it
      open(10,file='hhmutant-etaS1c7.dat')


      do ifile=1,50
      yrecNP=0.d0
      yrecWPI=0.d0
      yrecWPR=0.d0
      
      g0S=log(2.d0)*60.d0/24.d0
      g0B=g0S/1.19d0
      K=1.d9

      nstep=8
      etaB=60*6.5d-10
      etaS=etaB*1.7d0
      tau=24.d0*2.1d0/60.d0
      krate=dble(nstep)/tau
      dt=1.d-6
      tfinal=log(520.d0)/g0S
      beta=21.d0*2.8d0
c      write(10,*) '#g0S, g0B, nstep, etaS, etaB,tau, beta'
c      write(10,*) '#',g0S, g0B, nstep, etaS, etaB,tau, beta      
c     S/I+S=x-> S=xI/(1-x)
      xfrac=ifile*0.005d0
      Iinitial =1.4d7
      Sinitial=xfrac*Iinitial/(1.d0-xfrac)
      Ninitial= 4.5d9
      trec=0.1d0
      tnow=0.d0
cccc        simulation without phage
      do j=1,n
         y(j)=0.d0
      end do
      y(1)=Ninitial
      y(2)=Iinitial
      y(3)=Sinitial
      t=0.d0
      it=0
      do j=1,n
         yrecNP(j)=y(j)
      end do
      do while(t.lt.tfinal)
         call rk4(y,dydt,n,t,dt,yout,derivs)
         do j=1,n
            y(j)=yout(j)
         end do
         t=t+dt
      end do
      do j=1,n
         yrecNP(j)=y(j)
      end do      
cccc        simulation with phage, immune
      do j=1,n
         y(j)=0.d0
      end do
      y(1)=Ninitial
      y(2)=Iinitial
      y(3)=Sinitial
      y(4)=4.d4
      it=0
      do j=1,n
         yrecWPI(j)=y(j)
      end do      
      t=0.d0
      do while(t.lt.tfinal)
         call rk4(y,dydt,n,t,dt,yout,derivs)
         do j=1,n
            y(j)=yout(j)
         end do
         t=t+dt
      end do
      do j=1,n
         yrecWPI(j)=y(j)
      end do


cccc        simulation with phage, registant
      do j=1,n
         y(j)=0.d0
      end do
      y(1)=Ninitial
      y(2)=Iinitial
      y(3)=Sinitial
      y(4)=2.d5/5.d0
      t=0.d0
      it=0
      do j=1,n
         yrecWPR(j)=y(j)
      end do      
      do while(t.lt.tfinal)
         call rk4(y,dydt,n,t,dt,yout,derivsR)
         do j=1,n
            y(j)=yout(j)
         end do
         t=t+dt
         tnow=tnow+dt
         if(tnow.ge.trec)then
            it=it+1
            tnow=0.d0
         end if
      end do
      do j=1,n
         yrecWPR(j)=y(j)
      end do
      write(10,*) xfrac,
     &     yrecNP(3)/(yrecNP(3)+yrecNP(2)),
     &     yrecWPI(3)/(yrecWPI(3)+yrecWPI(2)),
     &     yrecWPR(3)/(yrecWPR(3)+yrecWPR(2))
      end do

      end
